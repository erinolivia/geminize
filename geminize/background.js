import { getProxyList } from './storage.js';

//                              Common Functions
////////////////////////////////////////////////////////////////////////////////

function escapeHTML(str) {
    return String(str)
        .replace(/&/g, '&amp;')
        .replace(/"/g, '&quot;').replace(/'/g, '&#39;')
        .replace(/</g, '&lt;').replace(/>/g, '&gt;');
}

var _gotoPromise;
function gotoUrl(tabId, url) {
    _gotoPromise = browser.tabs.update(tabId, { url: url });
}

// adapted from rfc 3986 & the gemini spec
const GeminiRe = /^gemini:\/\/(?<authority>[^@\/?#]*)(?<request>([^@?#]*)(\?([^#]*))?(#(.*))?)$/;

function validUrl(string) {
    return GeminiRe.exec(string) != null;
}

// perhaps unnecessary caching
var proxyList = [];
var _proxyPromise = getProxyList().then(list => proxyList = list);

function defaultProxy() {
    return proxyList.find(proxy => proxy.default);
}

function proxyUrl(geminiUrl, proxy) {
    // try to parse 'gemini://pathname' url
    const match = GeminiRe.exec(geminiUrl);
    if (!match) { throw 'Bad URL!'; }

    const pathname = match.groups.authority + match.groups.request;

    if (proxy != undefined) {
        return proxy + escapeHTML(pathname);
    }

    return defaultProxy().url + escapeHTML(pathname);
}

//                         Handle gemini:// Protocol
////////////////////////////////////////////////////////////////////////////////

browser.webNavigation.onBeforeNavigate.addListener(
    details => gotoUrl(details.tabId, proxyUrl(details.url)),
    { url: [{ schemes: ['gemini'] }] }
);

//                              Message Handler
////////////////////////////////////////////////////////////////////////////////

function handleMsg(request, sender, sendResponse) {
    if (request.selection != undefined) {
        handleSelection(request.selection);
    } else if (request.update != undefined) {
        _proxyPromise = getProxyList().then(list => proxyList = list);
    }
}

browser.runtime.onMessage.addListener(handleMsg);

//                                  Omnibox
////////////////////////////////////////////////////////////////////////////////

// fenix doesn't support omnibox
if (browser.omnibox) {

browser.omnibox.setDefaultSuggestion({
    description: 'Enter a gemini:// URL'
});

browser.omnibox.onInputChanged.addListener((input, suggest) => {
    // check for URLs without scheme
    let url = input;
    if (!validUrl(input) && validUrl('gemini://' + input)) {
        url = 'gemini://' + input;
    }

    if (validUrl(url)) {
        for (const proxy of proxyList) {
            suggest([{
                content: proxyUrl(url, proxy.url),
                description: proxy.name
            }]);
        }
    }

    suggest([]);
});

browser.omnibox.onInputEntered.addListener((input, disposition) => {
    let url = input;
    if (!validUrl(input) && validUrl('gemini://' + input)) {
        url = 'gemini://' + input;
    }

    // do nothing for invalid urls
    if (!validUrl(url)) {
        return;
    }

    url = proxyUrl(url);

    switch (disposition) {
        case 'currentTab':
            browser.tabs.update({url});
            break;
        case 'newForegroundTab':
            browser.tabs.create({url});
            break;
        case 'newBackgroundTab':
            browser.tabs.create({url, active: false});
            break;
    }
});

}

//                                 Onboarding
////////////////////////////////////////////////////////////////////////////////

browser.runtime.onInstalled.addListener(async (details) => {
    if (details.reason == 'install') {
        const url = browser.runtime.getURL('/installed.html');
        await browser.tabs.create({ url });
    }
});

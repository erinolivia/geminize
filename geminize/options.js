import { DefaultProxies } from './proxies.js';
import { DefaultOptions } from './storage.js';

function updateBackground() {
    browser.runtime.sendMessage({ update: true });
}

function saveOptions(e) {
    e.preventDefault();
    browser.storage.sync.set({
        customProxy: document.querySelector('#custom-proxy').value,
        proxyChoice: document.querySelector('input[name="proxy_choice"]:checked').value,
    });

    updateBackground();
}

function restoreOptions() {
    function onError(error) {
        console.log(`Error: ${error}`);
    }

    browser.storage.sync.get(DefaultOptions).then(
        result => {
            document.querySelector('#custom-proxy').value = result.customProxy;
            document.querySelector(`input[value="${result.proxyChoice}"]`).checked = true;
        },
        onError
    );
}

function setupProxies() {
    let proxyList = document.querySelector('#proxy-list');
    let id = 1;
    for (const proxy of DefaultProxies) {
        let group = document.createElement('div');

        let inputElem = document.createElement('input');
        inputElem.id = `input-${id}`;
        inputElem.type = 'radio';
        inputElem.value = proxy.name;
        inputElem.name = 'proxy_choice';
        group.appendChild(inputElem);

        let labelElem = document.createElement('label');
        labelElem.setAttribute('for', `input-${id}`);
        labelElem.appendChild(document.createTextNode(proxy.name));
        group.appendChild(labelElem);

        proxyList.prepend(group);
        id += 1;
    }
}

function init() {
    setupProxies();
    restoreOptions();
}

document.addEventListener('DOMContentLoaded', init);
document.querySelector('form').addEventListener('change', saveOptions);

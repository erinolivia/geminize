# Geminize

Explore Project Gemini sites in your browser! This web extension adds support
for accessing Project Gemini URLs and links (like
<gemini://gemini.circumlunar.space>) via a configurable HTTP-to-Gemini web
proxy.

Works on Firefox.

## Building

This project requires [yarn](https://yarnpkg.com/) to build. Development
dependencies are listed in `package.json`/`yarn.lock`.

To build, run `yarn build`. This will make a `dist` directory containing the
build products.

To test in firefox (via web-ext), run `yarn test`.
